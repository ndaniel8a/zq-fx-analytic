package com.zysquy.fx.analytic.be.ms.instrument.service;

import com.zysquy.fx.analytic.common.entity.Instrument;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instrument")
public class InstrumentEntity extends Instrument {


    @Override
    @Id
    public Integer getId() {
        return super.getId();
    }

    @Override
    @Column
    public String getCode() {
        return super.getCode();
    }

    @Override
    @Column
    public String getType() {
        return super.getType();
    }

    @Override
    @Column
    public String getDescripcion() {
        return super.getDescripcion();
    }

    @Override
    @Column
    public Boolean getEnabled() {
        return super.getEnabled();
    }

    @Override
    @Column(name = "code_broker")
    public String getCodeBroker() {
        return super.getCodeBroker();
    }

    @Override
    @Column(name = "enable_load_data_broker")
    public Boolean getEnableLoadDataBroker() {
        return super.getEnableLoadDataBroker();
    }


}
