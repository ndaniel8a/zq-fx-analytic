package com.zysquy.fx.analytic.be.ms.instrument.service;


import com.zysquy.fx.analytic.common.entity.Instrument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/instrument")
public class InstrumentServiceProvider {

    @Autowired
    private InstrumentRepository instrumentRepository;

    @GetMapping(value = "/enabled-data-broker")
    @ResponseBody
    public List<Instrument> getEnabledDataBroker() {
        return this.instrumentRepository.findByEnableLoadDataBrokerOrderByCode(true);
    }


    @GetMapping(value = "/enabled")
    @ResponseBody
    public List<Instrument> getEnabled() {
        return this.instrumentRepository.findByEnabledOrderByCode(true);
    }
}
