package com.zysquy.fx.analytic.be.ms.instrument.service;


import com.zysquy.fx.analytic.common.entity.Instrument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstrumentRepository extends JpaRepository<InstrumentEntity, Integer>  {


    List<Instrument> findByEnabledOrderByCode(boolean enabled);

    List<Instrument> findByEnableLoadDataBrokerOrderByCode(boolean enabled);

}
