package com.zysquy.fx.analytic.be.ms.instrument;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@Import({JpaConfig.class})
public class ZqFxAnalyticBeInstrumentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZqFxAnalyticBeInstrumentServiceApplication.class, args);
    }

}
