package com.zysquy.fx.analytic.be.broker.service.clients;


import com.zysquy.fx.analytic.common.entity.broker.oanda.InstrumentBrokerData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "broker-service-instrument", url = "${broker.url.get-data-instruments}")
public interface BrokerServiceInstrumentsClient {

    @RequestMapping( method = RequestMethod.GET, value = "/{instrumentId}/candles?granularity={periodId}&from={dateFrom}&to={dateTo}")
    InstrumentBrokerData getCandles(@PathVariable String instrumentId, @RequestParam String periodId,
                                    @RequestParam String dateFrom, @RequestParam String dateTo);
}
