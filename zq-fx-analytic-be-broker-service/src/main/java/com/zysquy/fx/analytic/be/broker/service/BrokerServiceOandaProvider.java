package com.zysquy.fx.analytic.be.broker.service;

import com.zysquy.fx.analytic.be.broker.service.clients.BrokerServiceInstrumentsClient;
import com.zysquy.fx.analytic.common.entity.broker.oanda.InstrumentBrokerData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/broker")
public class BrokerServiceOandaProvider implements BrokerService {



    @Autowired
    private BrokerServiceInstrumentsClient serviceInstrumentsClient;

    @Override
    @GetMapping(value = "/intrument-data")
    @ResponseBody
    public InstrumentBrokerData getData(String instrumentId, @RequestParam String periodId, @RequestParam String dateFrom, @RequestParam String dateTo) {
        return this.serviceInstrumentsClient.getCandles(instrumentId, periodId, dateFrom, dateTo);
    }

    @Override
    @PostMapping( value = "/load-data-replace")
    @ResponseBody
    public Integer loadDataAndReplace(String instrumentId, String periodId, String dateFrom, String dateTo) {

        this.getData(instrumentId, periodId, dateFrom, dateTo);

        return null;
    }

    @Override
    @PostMapping( value = "/load-data-append")
    @ResponseBody
    public Integer loadDataAndAppend(String instrumentId, String periodId, String dateFrom, String dateTo) {
        return null;
    }
}
