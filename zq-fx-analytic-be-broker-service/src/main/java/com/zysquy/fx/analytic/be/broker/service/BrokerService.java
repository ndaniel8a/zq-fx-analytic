package com.zysquy.fx.analytic.be.broker.service;

import com.zysquy.fx.analytic.common.entity.broker.oanda.InstrumentBrokerData;

public interface BrokerService {

    InstrumentBrokerData getData(String instrumentId, String periodId, String dateFrom, String dateTo);

    Integer loadDataAndReplace(String instrumentId, String periodId, String dateFrom, String dateTo);

    Integer loadDataAndAppend(String instrumentId, String periodId, String dateFrom, String dateTo);

}
