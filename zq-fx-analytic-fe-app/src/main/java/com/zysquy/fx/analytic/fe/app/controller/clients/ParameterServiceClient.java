package com.zysquy.fx.analytic.fe.app.controller.clients;

import com.zysquy.fx.analytic.common.entity.Instrument;
import com.zysquy.fx.analytic.common.entity.Parameter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name="analytic-parameter-service")
public interface ParameterServiceClient {

    @RequestMapping("/parameter/category/fx_periods")
    List<Parameter> getFxPeriods();

}

