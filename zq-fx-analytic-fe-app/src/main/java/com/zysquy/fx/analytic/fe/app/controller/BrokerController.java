package com.zysquy.fx.analytic.fe.app.controller;

import com.zysquy.fx.analytic.common.entity.Instrument;
import com.zysquy.fx.analytic.common.entity.Parameter;
import com.zysquy.fx.analytic.fe.app.controller.clients.InstrumentServiceClient;
import com.zysquy.fx.analytic.fe.app.controller.clients.ParameterServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(path = "/api/broker")
public class BrokerController {

    @Autowired
    private InstrumentServiceClient instrumentServiceClient;

    @Autowired
    private ParameterServiceClient parameterServiceClient;



    @GetMapping(value = "/instrument")
    @ResponseBody
    public List<Instrument> getInstruments() {
        return instrumentServiceClient.getEnabledDataBroker();
    }

    @GetMapping(value = "/fx-periods")
    @ResponseBody
    public List<Parameter> getFXPeriods() {
        return parameterServiceClient.getFxPeriods();
    }

    @GetMapping(value = "/load-data-replace")
    @ResponseBody
    public Boolean loadDataAndReplace(@RequestParam Integer instrumentId, @RequestParam Integer periodId, @RequestParam String stDateIni, @RequestParam String stDateFin) {

        System.out.println("Load data...." + instrumentId + " " + periodId );

        return  true;
    }

}
