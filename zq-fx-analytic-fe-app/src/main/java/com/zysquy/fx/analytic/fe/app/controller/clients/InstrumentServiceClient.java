package com.zysquy.fx.analytic.fe.app.controller.clients;

import com.zysquy.fx.analytic.common.entity.Instrument;
//import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "analytic-instrument-service")
public interface InstrumentServiceClient {

    @RequestMapping("/instrument/enabled-data-broker")
    List<Instrument> getEnabledDataBroker();

    @RequestMapping("/instrument/enabled")
    List<Instrument> getEnabled();
}
