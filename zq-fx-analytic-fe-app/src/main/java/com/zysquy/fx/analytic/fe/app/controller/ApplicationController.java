    package com.zysquy.fx.analytic.fe.app.controller;


import com.zysquy.fx.analytic.common.entity.Instrument;
import com.zysquy.fx.analytic.fe.app.controller.clients.InstrumentServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(path = "/api/application")
public class ApplicationController {

    @Autowired
    private InstrumentServiceClient instrumentClient;

    @Autowired
    private DiscoveryClient discoveryClient;


    @GetMapping(value = "/instrument")
    @ResponseBody
    public List<Instrument> getInstruments() {
        discoveryClient.getServices();
        return instrumentClient.getEnabled();
    }
}
