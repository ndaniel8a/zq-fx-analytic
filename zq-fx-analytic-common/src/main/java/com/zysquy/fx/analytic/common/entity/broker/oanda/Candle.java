package com.zysquy.fx.analytic.common.entity.broker.oanda;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Candle {

    private Boolean complete;

    private Integer volume;

    private String time;

    private CandleValue mid;

    private CandleValue ask;

}
