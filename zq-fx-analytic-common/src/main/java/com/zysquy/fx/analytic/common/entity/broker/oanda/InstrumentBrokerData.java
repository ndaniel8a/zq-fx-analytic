package com.zysquy.fx.analytic.common.entity.broker.oanda;


import lombok.*;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class InstrumentBrokerData {

    private String instrument;

    private String granularity;

    private List<Candle> candles;
}
