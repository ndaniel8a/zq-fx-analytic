package com.zysquy.fx.analytic.common.entity;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Instrument implements Serializable {

    private Integer id;

    private String code;

    private String type;

    private String descripcion;

    private Boolean enabled;

    private String codeBroker;

    private Boolean enableLoadDataBroker;

}
