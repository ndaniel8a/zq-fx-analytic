package com.zysquy.fx.analytic.common.entity;

import lombok.*;

import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class InstrumentData implements Serializable {

    private Long id;
    private Integer instrumentId;
    private Instrument instrument;
    private Date date;
    private Float open;
    private Float high;
    private Float close;
    private Float low;
    private String period;
    private Date createDate;
    private Integer volume;
    private String broker;

}
