package com.zysquy.fx.analytic.common.entity.broker.oanda;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class CandleValue {

    private Double o;

    private Double h;

    private Double l;

    private Double c;
}
