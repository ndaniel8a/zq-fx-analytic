package com.zysquy.fx.analytic.common.entity;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Parameter implements Serializable {

    private Integer id;
    private String code;
    private String category;
    private Short ord;
    private String descripcion;
    private String valor;

}
