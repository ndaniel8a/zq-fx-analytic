package com.zysquy.fx.analytic.be.parameter.service;

import com.zysquy.fx.analytic.common.entity.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parameter")
public class ParameterEntity extends Parameter {

    @Override
    @Id
    public Integer getId() {
        return super.getId();
    }

    @Override
    @Column
    public String getCode() {
        return super.getCode();
    }

    @Override
    @Column
    public String getCategory() {
        return super.getCategory();
    }

    @Override
    @Column
    public Short getOrd() {
        return super.getOrd();
    }

    @Override
    @Column
    public String getDescripcion() {
        return super.getDescripcion();
    }

    @Override
    @Column
    public String getValor() {
        return super.getValor();
    }
}
