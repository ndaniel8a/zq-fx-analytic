package com.zysquy.fx.analytic.be.parameter.service;

import com.zysquy.fx.analytic.common.entity.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping(path = "/parameter")
public class ParameterServiceProvider implements ParameterService {

    @Autowired
    private ParameterRepository parameterRepository;

    @GetMapping(value = "/all")
    @ResponseBody
    public Parameter getAll() {
        return Parameter.builder().code("XXXXX").id(2).build();
    }

    @Override
    @GetMapping(value = "/category/{category}")
    @ResponseBody
    public List<Parameter> getByCategory(@PathVariable String category) {
        return this.parameterRepository.findByCategoryOrderByOrd( (category != null ? category.toUpperCase() : null) );
    }

    @Override
    @GetMapping(value = "/code/{code}")
    @ResponseBody
    public Parameter getByCode(@PathVariable String code) {
        return this.parameterRepository.findByCode(code);
    }

    @Override
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Parameter getById(@PathVariable Integer id) {
        return this.parameterRepository.findById(id);
    }
}
