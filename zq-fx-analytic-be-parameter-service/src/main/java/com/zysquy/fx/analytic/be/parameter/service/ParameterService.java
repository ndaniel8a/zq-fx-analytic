package com.zysquy.fx.analytic.be.parameter.service;

import com.zysquy.fx.analytic.common.entity.Parameter;

import java.util.List;

public interface ParameterService {

    List<Parameter> getByCategory(String category);

    Parameter getByCode(String code);

    Parameter getById(Integer id);
}
