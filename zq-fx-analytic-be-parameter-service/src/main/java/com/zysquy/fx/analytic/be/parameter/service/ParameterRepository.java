package com.zysquy.fx.analytic.be.parameter.service;

import com.zysquy.fx.analytic.common.entity.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParameterRepository extends JpaRepository<ParameterEntity, String> {

    List<Parameter> findByCategoryOrderByOrd(String category);

    Parameter findByCode(String code);

    Parameter findById(Integer id);

}
