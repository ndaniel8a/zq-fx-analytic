package com.zysquy.fx.analytic.be.parameter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@Import({JpaConfig.class})
public class ZqFxAnalyticBeParameterServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZqFxAnalyticBeParameterServiceApplication.class, args);
    }

}
