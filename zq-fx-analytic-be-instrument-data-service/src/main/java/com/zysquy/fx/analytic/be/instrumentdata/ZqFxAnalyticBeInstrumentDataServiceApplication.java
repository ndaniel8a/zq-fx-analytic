package com.zysquy.fx.analytic.be.instrumentdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@Import({JpaConfig.class})
public class ZqFxAnalyticBeInstrumentDataServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZqFxAnalyticBeInstrumentDataServiceApplication.class, args);
    }

}
