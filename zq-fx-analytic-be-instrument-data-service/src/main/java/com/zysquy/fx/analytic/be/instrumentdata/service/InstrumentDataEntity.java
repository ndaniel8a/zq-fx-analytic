package com.zysquy.fx.analytic.be.instrumentdata.service;

import com.zysquy.fx.analytic.common.entity.Instrument;
import com.zysquy.fx.analytic.common.entity.InstrumentData;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "instrument_data")
public class InstrumentDataEntity extends InstrumentData {

    @Override
    @Id
    public Long getId() {
        return super.getId();
    }

    @Override
    @Column(name = "instrument_id")
    public Integer getInstrumentId() {
        return super.getInstrumentId();
    }

    @Override
    public Instrument getInstrument() {
        return super.getInstrument();
    }

    @Override
    public Date getDate() {
        return super.getDate();
    }

    @Override
    public Float getOpen() {
        return super.getOpen();
    }

    @Override
    public Float getHigh() {
        return super.getHigh();
    }

    @Override
    public Float getClose() {
        return super.getClose();
    }

    @Override
    public Float getLow() {
        return super.getLow();
    }

    @Override
    public String getPeriod() {
        return super.getPeriod();
    }

    @Override
    @Column(name = "create_date")
    public Date getCreateDate() {
        return super.getCreateDate();
    }

    @Override
    public Integer getVolume() {
        return super.getVolume();
    }

    @Override
    public String getBroker() {
        return super.getBroker();
    }
}
