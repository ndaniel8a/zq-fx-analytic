package com.zysquy.fx.analytic.be.instrumentdata.service;

import com.zysquy.fx.analytic.common.entity.InstrumentData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface InstrumentDataRepository extends JpaRepository<InstrumentDataEntity, Integer> {

    List<InstrumentData> findByInstrumentIdAndPeriodAndDateBetweenOrderByDate(Integer instrumentId, String period, Date dateIni, Date dateFin);
}
