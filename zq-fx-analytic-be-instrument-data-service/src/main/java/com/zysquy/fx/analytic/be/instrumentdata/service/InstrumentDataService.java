package com.zysquy.fx.analytic.be.instrumentdata.service;

import com.zysquy.fx.analytic.common.entity.InstrumentData;

import java.util.List;

public interface InstrumentDataService {

    void save(List<InstrumentData> instrumentData);

}
