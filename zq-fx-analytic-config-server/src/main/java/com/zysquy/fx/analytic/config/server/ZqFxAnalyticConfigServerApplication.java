package com.zysquy.fx.analytic.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ZqFxAnalyticConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZqFxAnalyticConfigServerApplication.class, args);
    }

}
