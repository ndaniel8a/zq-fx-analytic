import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Section} from './app/section-list/Section';
import { HttpClient } from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {


  private urlApiApp =  environment['url.api'].concat('/application');
  private urlApiAppSection = this.urlApiApp.concat('/section');

  constructor(private http: HttpClient) {
  }

  getSections(): Observable<Array<Section>> {

    return this.http.get<Section[]>(this.urlApiAppSection).pipe(
      catchError(this.handleError('getSections', []) )
    );

  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

