import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrokerLoadDataComponent} from "./broker/broker-load-data/broker-load-data.component";

const routes:Routes = [
  {path: 'broker-load-data', component: BrokerLoadDataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
