import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from "@angular/forms";
import { registerLocaleData } from "@angular/common";
import  localeEs from "@angular/common/locales/es";

import { MatCardModule } from '@angular/material';
import { MatInputModule } from "@angular/material";
import { MatSelectModule } from "@angular/material";
import { MatDatepickerModule } from "@angular/material";
import { MatNativeDateModule} from "@angular/material";

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { SectionListComponent } from './app/section-list/section-list.component';
import { BrokerLoadDataComponent } from "./broker/broker-load-data/broker-load-data.component";



registerLocaleData(localeEs, 'es-CO');

@NgModule({
  declarations: [
    AppComponent,
    SectionListComponent,
    BrokerLoadDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    NgbModule,
    HttpClientModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {

}
