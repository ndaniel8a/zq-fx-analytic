export class Parameter {
  id: number;
  code: string;
  category: string;
  ord: number;
  descripcion: string;
  valor: string;
}
