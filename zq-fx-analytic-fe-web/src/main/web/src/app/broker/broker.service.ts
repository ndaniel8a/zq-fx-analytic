import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams} from "@angular/common/http";
import {catchError} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {Instrument} from "./Instrument";
import {Parameter} from "../parameter/Parameter";
import {formatDate} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class BrokerService {

  private urlApiApp =  environment['url.api'].concat('/broker');
  private urlAPIInstrument = this.urlApiApp.concat('/instrument');
  private urlAPIFXPeriods = this.urlApiApp.concat('/fx-periods');
  private urlAPIBrokerData = this.urlApiApp.concat('/load-data-replace');

  constructor(private http: HttpClient) {
  }

  getInstruments(): Observable<Array<Instrument>> {

    return this.http.get<Instrument[]>(this.urlAPIInstrument).pipe(
      catchError(this.handleError('getInstruments', []) )
    );

  }

  getFXPeriods(): Observable<Array<Parameter>> {
    return this.http.get<Parameter[]>(this.urlAPIFXPeriods).pipe(
      catchError(this.handleError('getFXPeriods', []))
    );
  }

  loadDataFromBroker(instrumentId :number, periodId: number, dateIni: Date, dateFin: Date, replace:boolean): Observable<Boolean> {

    let httpOptions = {
      params : new HttpParams()
        .set('instrumentId', instrumentId.toString())
        .set('periodId', periodId.toString())
        .set('dateIni', formatDate(dateIni, 'dd/MM/YYYY', environment["locale.default"] ))
        .set('dateFin', formatDate(dateIni, 'dd/MM/YYYY', environment["locale.default"] ))
    };

    return this.http.post<Boolean>(this.urlAPIBrokerData, {}, httpOptions).pipe(
      catchError(this.handleError('loadDataFromBroker', false))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
