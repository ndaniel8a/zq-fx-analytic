import { Component, OnInit } from '@angular/core';
import { Instrument } from "../Instrument";
import {BrokerService} from "../broker.service";
import {Parameter} from "../../parameter/Parameter";
import {FormControl} from "@angular/forms";
import { FormBuilder } from "@angular/forms";


@Component({
  selector: 'broker-load-data',
  templateUrl: './broker-load-data.component.html',
  styleUrls: ['./broker-load-data.component.css']
})


export class BrokerLoadDataComponent implements OnInit {

  instruments: Instrument[];
  periods: Parameter[];





  minDate = new Date(2015, 0, 1);
  maxDate = new Date();

  constructor(private brokerService: BrokerService, private fb: FormBuilder) { }

  brokerDataLoadForm = this.fb.group({
    instrument : [''],
    period: [''],
    dateIni: [''],
    dateFin: ['']
  });


  ngOnInit() {
    this.getInstruments();
    this.getFxPeriod();
  }

  getInstruments() {
    this.brokerService.getInstruments().subscribe(instruments => this.instruments = instruments );
  }

  getFxPeriod() {
    this.brokerService.getFXPeriods().subscribe(periods => this.periods = periods);
  }

  onSubmit() {
    console.warn('Valor Forma', this.brokerDataLoadForm);
  }

}
