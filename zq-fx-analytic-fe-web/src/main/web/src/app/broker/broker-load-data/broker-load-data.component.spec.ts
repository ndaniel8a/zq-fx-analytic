import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrokerLoadDataComponent } from './broker-load-data.component';

describe('BrokerLoadDataComponent', () => {
  let component: BrokerLoadDataComponent;
  let fixture: ComponentFixture<BrokerLoadDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrokerLoadDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrokerLoadDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
