export class Instrument {
  id: number;
  code: string;
  type: string;
  descripcion: string;
  enabled: boolean;
  codeBroker: string;
  enableLoadDataBroker: boolean;
}
