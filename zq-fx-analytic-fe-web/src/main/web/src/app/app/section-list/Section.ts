export class Section {
  id: number;
  uuid: string;
  title: string;
  subtitle: string;
  description: string;
  image: string;
}
