import { Component, OnInit } from '@angular/core';
import {Section} from './Section';
import {ApplicationService} from '../../application.service';

@Component({
  selector: 'app-section-list',
  templateUrl: './section-list.component.html',
  styleUrls: ['./section-list.component.css']
})
export class SectionListComponent implements OnInit {

  sections: Section[];

  constructor(private applicationService: ApplicationService) {
  }


  ngOnInit() {
    this.getSections();
  }

  getSections() {
    this.applicationService.getSections().subscribe( sections => this.sections = sections);
  }

}
